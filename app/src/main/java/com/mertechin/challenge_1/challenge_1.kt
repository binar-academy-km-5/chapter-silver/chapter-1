package com.mertechin.challenge_1

fun main() {
    // Masukkan Data dalam ArrayList
    val daftarMenu = ArrayList<String>()
    daftarMenu.add("Ayam Bakar")
    daftarMenu.add("Ayam Goreng")
    daftarMenu.add("Ayam Geprek")
    daftarMenu.add("Kulit Ayam Crispy")
    daftarMenu.add("Sate Usus Ayam")

    val daftarHarga = ArrayList<Int>()
    daftarHarga.add(50000)
    daftarHarga.add(40000)
    daftarHarga.add(40000)
    daftarHarga.add(15000)
    daftarHarga.add(5000)

    val metodePengiriman = arrayListOf("Takeaway", "Delivery")

    // Introduction
    println("Selamat Datang di warung Binar!\n")
    pembatas()
    println("Daftar Menu Unggulan di Warung Binar:")
    println()

    // Untuk Menampilkan Keseluruhan Menu dan Harga
    for (i in 0 until daftarMenu.size) {
        print(i)
        println(". ${daftarMenu[i]} -> Rp. ${daftarHarga[i]}")
    }
    pembatas()

    print("Masukkan Pilihanmu (dalam angka): ")
    val pilMenu = inputPilihan().toString()
    pilihanMenu(pilMenu, daftarMenu, daftarHarga)

    pembatas()

    // Melakukan pembayaran sesuai pilihan
    println("Lakukan Pembayaran kuy! Bayar pake uang pas ya!")
    print("Masukkan Nominal pembayaranmu: Rp. ")
    val status = statusPembayaran(inputPilihan().toString().toInt(), pilMenu, daftarHarga)

    if (status == 1) {
        println("Terima Kasih! Pembayaranmu Berhasil!")
        pembatas()
        println("Pilih metode pengirimanmu")

        for (i in 0 until metodePengiriman.size) {
            print(i)
            println(". ${metodePengiriman[i]}")
        }

        print("\n Masukkan pilihan metode pengirimanmu (Dengan Angka) : ")
        val pilihan = inputPilihan().toString().toInt()

        pembatas()

        metodePengiriman(pilihan)
    } else {
        println("Maaf, Pembayaran Anda Gagal!")
    }

}

// Untuk mengatur waktu loading sesuai dengan kebutuhan
fun loading(i: Int) {
    var startTime = System.currentTimeMillis()
    var iteration = 0

    while (iteration <= i) {
        val currentTime = System.currentTimeMillis()
        if (currentTime - startTime >= 1000) {
            print(".")

            startTime = currentTime
            iteration++
        }
    }
}

// untuk memasukkan pilihan
fun inputPilihan(): String? {
    val pilihan = readLine()
    return pilihan
}

// Menampilkan menu beserta harga yang dipilih oleh user
fun pilihanMenu(pil: String, menu: ArrayList<String>, harga: ArrayList<Int>) {
    println("Kamu Memilih Menu : $pil")
    println("${menu[pil.toInt()]} -> Rp. ${harga[pil.toInt()]}")
}

// Untuk Pembatas aja si hehehehe
fun pembatas() {
    println("==================================")
}

// Cek Status Pembayaran
fun statusPembayaran(money: Int, pil: String, harga: ArrayList<Int>): Int {
    if (money == harga[pil.toInt()]) {
        return 1
    } else {
        return 0
    }
}

// Pilih Metode Pengiriman
fun metodePengiriman(pil: Int) {
    when (pil) {
        1 -> {
            print("Makananmu Sedang dimasak (5 detik)")
            loading(5)
            print("\nMakananmu Sudah Siap! Silahkan diambil di resto (5 detik)")
            loading(5)
            print("\nPesanan Selesai (3 detik)")
            loading(3)
        }
        2 -> {
            print("Makananmu Sedang dimasak (5 detik)")
            loading(5)
            print("\n Makananmu Sudah Siap! Driver akan segera ke tempatmu (5 detik)")
            loading(5)
            print("\n Driver Sampai! Pesanan Selesai (3 detik)")
            loading(3)
        }
    }
}
